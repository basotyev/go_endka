generate:
	go test -bench . -benchmem -cpuprofile=cpu.out -memprofile=mem.out -memprofilerate=1 main_test.go

profcpu:
	go tool pprof main.test.exe cpu.out

profmem:
	go tool pprof main.test.exe mem.out



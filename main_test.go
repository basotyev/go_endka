package main

import (
	"bufio"
	"bytes"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	"testing"
	"time"
)
type Record struct {
	word    []byte
	counter int
	checked bool
}

//type Writer struct {
//	writingBuf *[]byte
//}
//
//func (w *Writer) writeToTempBuf(byte byte) {
//	*w.writingBuf = append(*w.writingBuf, byte)
//	//fmt.Println("WRITER: Written bytes to writing_buf", byte)
//}
//
//func (w *Writer) writeToChan(ch chan []byte) {
//	ch <- *w.writingBuf
//	//fmt.Println("WRITER: Send byte slice to chan", writing_buf)
//	*w.writingBuf = nil
//}

type Reader struct {
	words  *[]Record
	rating *[]int
}

func (r *Reader) contains(element []byte) (bool, int) {
	for index, v := range *r.words {
		if bytes.Equal(v.word, element) {
			return true, index
		}
	}
	return false, 0
}

func (r *Reader) readFromChan(ch chan []byte) {
	for node := range ch {
		state, index := r.contains(node)
		if state {
			(*r.words)[index].counter++
		} else {
			record := Record{node, 1, false}
			*r.words = append(*r.words, record)
		}
	}
}

func (r *Reader) getMostFreq() {
	list := make([]int, 20)
	r.rating = &list
	for i := 0; i < 20; i++ {
		temp := 0
		inss := 0
		for index, v := range *r.words {
			if (v.checked == false) && (v.counter > temp) {
				temp = v.counter
				inss = index
			}
		}
		(*r.words)[inss].checked = true
		(*r.rating)[i] = inss
	}
}

func (r *Reader) print() {
	for _, v := range *r.rating {
		fmt.Println((*r.words)[v].counter, " ", string((*r.words)[v].word))
	}
}

func Moby(out io.Writer) {
	start := time.Now()

	readingBuf := make([]byte, 1) //read file by one letter only

	words := make([]Record, 0)
	r := Reader{words: &words} //creating r

	writingBuf := make([]byte, 0)
	//w := Writer{&writingBuf} //w

	ch := make(chan []byte) //channel that we will use to pass slices of bytes from w to r
	//btw r listens in range of elements that are passed to channel, it will stop working when there are no elements left, so we don't need any wait groups

	go func() {
		file, err := os.Open("mobydick.txt") //open file
		if err != nil {
			log.Fatal(err)
		}
		defer func(file *os.File) {
			err := file.Close()
			if err != nil {
				panic(err)
			}
		}(file)
		r := bufio.NewReader(file)
		for {
			//reading file's letters one by one
			//_, err := file.Read(readingBuf)
			_, err := r.Read(readingBuf)
			if err != nil {
				fmt.Println(err)
				break
			}
			if err == io.EOF  {
				ch <- writingBuf
				writingBuf = nil
				//w.writeToChan(ch) //send temporary buffer content to channel, empty the temporary buffer
				break
			}
			byteVal := readingBuf[0]
			if byteVal >= 97 && byteVal <= 122 { //if symbol is lowercase letter
				writingBuf = append(writingBuf, byteVal)
				//w.writeToTempBuf(byteVal) //writing to temporary buffer
				continue
			} else if byteVal == 32 && len(writingBuf) != 0 { //if symbol is [space], and we have letters in our buffer
				ch <- writingBuf
				writingBuf = nil
				//w.writeToChan(ch) //send temporary buffer content to channel, empty the temporary buffer
			} else if byteVal >= 65 && byteVal <= 90 { //if symbol is uppercase letter
				byteVal = byteVal + 32
				writingBuf = append(writingBuf, byteVal)
				//w.writeToTempBuf(byteVal) //writing to temporary buffer
			} else if ((byteVal > 122 || byteVal < 65) || (byteVal > 90 && byteVal < 97)) && len(writingBuf) != 0 { //if symbol is any other than letter or space, and we have letters in our buffer
				ch <- writingBuf
				writingBuf = nil
				//w.writeToChan(ch) //send temporary buffer content to channel, empty the temporary buffer
			}
		}
		close(ch) //close channel, so our that our r will stop working after there are no elements left, in other case r will cause deadlock
	}()

	r.readFromChan(ch) //reading from channel in range of elements in channel

	r.getMostFreq() //getting 20 most frequent words, and write it to rating slice
	r.print()       //print elements from words according to the rating list
	//fmt.Fprintf(out, "Process took: %s\n\n", time.Since(start))
	fmt.Println("Process took:", time.Since(start))
}

func BenchmarkMoby(b *testing.B) {
	for i := 0; i < b.N; i++ {
		Moby(ioutil.Discard)
	}
}